module.exports = {
  env: {
    node: true,
    es6: true,
  },
  extends: [
    'eslint:recommended',
    'eslint-config-prettier',
    'plugin:import/recommended',
  ],
  plugins: ['eslint-plugin-prettier'],
  parserOptions: {
    ecmaVersion: 8,
    sourceType: 'module',
  },
  rules: {
    semi: ['error', 'never'],
    quotes: ['error', 'single'],
    camelcase: 'warn',
    'no-shadow': 'off',
    //"no-console": ["error", { allow: ["warn", "error"] }],
    'prettier/prettier': ['warn'],
    'consistent-return': 'warn',
    'import/prefer-default-export': 'warn',
    'class-methods-use-this': 'warn',
    'no-underscore-dangle': 'warn',
    'no-param-reassign': 'warn',
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
      },
    ],
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'import/no-unresolved': 'warn',
  },
}
