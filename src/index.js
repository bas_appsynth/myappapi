import express from 'express'
import bodyparser from 'body-parser'
import routes from './routes'
import 'dotenv/config'
import initialDatabase from './helper/db'
import logger from './middleware/logger'

let port = process.env.PORT
initialDatabase()

let app = express()
app.use(express.urlencoded({ extended: false }))
app.use(bodyparser.json())
app.use(logger, routes)
app.listen(port, () => console.log('Server - Started! listening Port: ' + port))

export default app
