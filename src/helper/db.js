import mongoose from 'mongoose'

export default function initialDatabase() {
  mongoose.connect(
    process.env.MONGODB_URL,
    { useNewUrlParser: true },
    (error) => {
      if (error) {
        console.log(error)
      } else {
        console.log('MongoDB - Connected!')
      }
    }
  )
}
