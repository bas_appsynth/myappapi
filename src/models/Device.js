import mongoose from 'mongoose'

export default mongoose.model(
  'Device',
  new mongoose.Schema({
    name: String,
    serial: String,
  })
)
