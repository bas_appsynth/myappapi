import mongoose from 'mongoose'

export default mongoose.model(
  'User',
  new mongoose.Schema({
    firstname: String,
    lastname: String,
    email: String,
    password: String,
  })
)
