import express from 'express'
import User from '../models/User'
import Device from '../models/Device'

const router = express.Router()

router.get('/', async (req, res) => {
  try {
    let email = req.user.email
    let user = await User.findOne({ email: email })
    if (!user) {
      return res.status(401).send({ error: 'User not found' })
    }
    return res.json(user.devices)
  } catch (e) {
    return res.status(500).send({ error: 'Service error' })
  }
})

router.post('/', async (req, res) => {
  try {
    let email = req.user.email
    let body = req.body
    let name = body.name
    let serial = body.serial
    if (!(email && body && name && serial)) {
      return res.status(400).send({ error: 'Data not formatted properly' })
    }

    let user = await User.findOne({ email: email })
    if (!user) {
      return res.status(401).send({ error: 'User not found' })
    }

    let device = new Device({ name: name, serial: serial })

    let devices = user.devices || {}
    devices.push(device)
    user.devices = devices
    await user.save()
    return res.sendStatus(201)
  } catch (e) {
    return res.status(500).send({ error: 'Service error' })
  }
})

export default router
