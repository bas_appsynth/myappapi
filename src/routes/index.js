import express from 'express'
import auth from './auth'
import device from './device'
import authentication from '../middleware/authentication'

const router = express.Router()
router.use('/auth', auth)
router.use('/device', authentication, device)

export default router
