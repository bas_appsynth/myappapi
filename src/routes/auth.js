import express from 'express'
import User from '../models/User'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

const router = express.Router()

router.post('/signup', async (req, res) => {
  try {
    const body = req.body
    if (!(body.firstname && body.lastname && body.email && body.password)) {
      return res.status(400).send({ error: 'Data not formatted properly' })
    }

    let user = await User.findOne({ email: body.email })
    if (user) {
      return res.status(400).send({ error: 'Email already exists' })
    }

    let salt = await bcrypt.genSalt()
    let hasPassword = await bcrypt.hash(body.password, salt)

    await User.create({
      firstname: body.firstname,
      lastname: body.lastname,
      email: body.email,
      password: hasPassword,
    })

    return res.status(201).send({ message: 'SignUp sucess' })
  } catch (e) {
    return res.status(500).send({ error: 'Service error' })
  }
})

router.post('/login', async (req, res) => {
  try {
    const body = req.body
    if (!(body.email && body.password)) {
      return res.status(400).send({ error: 'Data not formatted properly' })
    }

    let user = await User.findOne({ email: body.email })
    if (!user) {
      return res.status(404).send({ error: 'User not found' })
    }

    const isValidPassword = await bcrypt.compare(body.password, user.password)
    if (isValidPassword) {
      let accessToken = jwt.sign(
        { email: user.email },
        process.env.ACCESS_TOKEN_SECRET
      )
      return res.status(200).json({ accessToken: accessToken })
    } else {
      return res.status(401).send({ error: 'Password invalid' })
    }
  } catch (e) {
    return res.status(500).send({ error: 'Service error' })
  }
})

export default router
