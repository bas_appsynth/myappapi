export default (req, res, next) => {
  console.log('url: ' + req.originalUrl)
  console.log('headers: ' + JSON.stringify(req.headers))
  console.log('body: ' + JSON.stringify(req.body))
  next()
}
