import User from '../models/User'

async function create() {
  let user = new User({
    firstname: 'test',
  })
  await user.save()
}

export default { create }
